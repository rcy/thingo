start: node_modules
	npm start

node_modules:
	${MAKE} install

install:
	npm install

build: node_modules
	npm run build

deploy: build
	npx firebase deploy

TMP:=$(shell mktemp)

# dbset-instructions | dbset-lexicon
dbset-%: thingo_schema.json
	jq .games.magic.$* $< > ${TMP}
	npx --no-install firebase database:set --confirm /games/magic/$* ${TMP}

# WARN: this will clobber the whole database!
dbset: thingo_schema.json
	npx --no-install firebase database:set --confirm / $<

clean:
	rm -rf build node_modules
